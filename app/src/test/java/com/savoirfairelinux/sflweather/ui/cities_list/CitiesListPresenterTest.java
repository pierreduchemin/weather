package com.savoirfairelinux.sflweather.ui.cities_list;

import android.content.Context;

import com.savoirfairelinux.sflweather.data.DataManager;
import com.savoirfairelinux.sflweather.data.file.model.City;
import com.savoirfairelinux.sflweather.utils.rx.TestSchedulerProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CitiesListPresenterTest {

    @Mock
    CitiesListMvpView mockCitiesListMvpView;
    @Mock
    DataManager mockDataManager;
    @Mock
    Context mockContext;

    private CitiesListPresenter<CitiesListMvpView> citiesListMvpViewCitiesListPresenter;
    private TestScheduler testScheduler;

    @Before
    public void setUp() throws Exception {
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        testScheduler = new TestScheduler();
        TestSchedulerProvider testSchedulerProvider = new TestSchedulerProvider(testScheduler);
        citiesListMvpViewCitiesListPresenter = new CitiesListPresenter<>(
                mockDataManager,
                testSchedulerProvider,
                compositeDisposable);
        citiesListMvpViewCitiesListPresenter.onAttach(mockCitiesListMvpView);
    }

    @Test
    public void onLoadCities() throws Exception {

        City montréal = new City("1", "Montréal");
        when(mockDataManager.getCitiesFromFile(mockContext))
                .thenReturn(Observable.just(montréal));

        citiesListMvpViewCitiesListPresenter.onLoadCities(mockContext);

        testScheduler.triggerActions();

        verify(mockCitiesListMvpView).showLoading();
        verify(mockCitiesListMvpView).hideLoading();
        verify(mockCitiesListMvpView, times(2)).setCityList(anyListOf(City.class));
    }

    @After
    public void tearDown() throws Exception {
        citiesListMvpViewCitiesListPresenter.onDetach();
    }
}
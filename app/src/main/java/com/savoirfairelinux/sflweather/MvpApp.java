package com.savoirfairelinux.sflweather;

import android.app.Application;
import android.os.StrictMode;

import com.savoirfairelinux.sflweather.di.ApplicationComponent;
import com.savoirfairelinux.sflweather.di.ApplicationModule;
import com.savoirfairelinux.sflweather.di.DaggerApplicationComponent;

import timber.log.Timber;

public class MvpApp extends Application {

    private static MvpApp instance;
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }

        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static MvpApp getInstance() {
        return instance;
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }
}

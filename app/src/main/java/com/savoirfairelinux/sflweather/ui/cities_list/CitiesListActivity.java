package com.savoirfairelinux.sflweather.ui.cities_list;

import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.savoirfairelinux.sflweather.R;
import com.savoirfairelinux.sflweather.data.file.model.City;
import com.savoirfairelinux.sflweather.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import timber.log.Timber;

public class CitiesListActivity extends BaseActivity implements CitiesListMvpView {

    @Inject
    CitiesListMvpPresenter<CitiesListMvpView> citiesListMvpPresenter;

    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.rvCities)
    RecyclerView rvCities;
    @BindView(R.id.pbCities)
    ProgressBar pbCities;

    private CityAdapter cityAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities_list);

        getAppComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        citiesListMvpPresenter.onAttach(this);

        setUp();
    }

    @Override
    protected void onResume() {
        super.onResume();

        citiesListMvpPresenter.onLoadCities(this);
    }

    @Override
    public void onNext(City city) {
        Timber.d("onNext(%s)", city);

        cityAdapter.addCity(city);
    }

    @Override
    public void onFileReadError() {
        showMessage(R.string.error_message_not_able_to_read_file);
    }

    @Override
    public void setCityList(List<City> allCities) {
        cityAdapter = new CityAdapter(allCities);
        rvCities.setAdapter(cityAdapter);
    }

    @Override
    public void setMaxProgressBar(int fileSize) {
        pbCities.setMax(fileSize);
    }

    @Override
    protected void setUp() {
        rvCities.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvCities.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvCities.setHasFixedSize(true);

        setCityList(new ArrayList<>());
    }

    @OnTextChanged(R.id.etCity)
    protected void onTextChanged(CharSequence text) {
        if (text.length() == 0) {
            return;
        }
        Timber.d("Filtering on %s", text);
        cityAdapter.filter(text.toString());
    }

    @Override
    public void showLoading() {
        etCity.setEnabled(false);
        pbCities.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateProgress(@IntRange(from = 0) int progress) {
        pbCities.setProgress(progress);
    }

    @Override
    public void hideLoading() {
        etCity.setEnabled(true);
        pbCities.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onDestroy() {
        citiesListMvpPresenter.onDetach();
        super.onDestroy();
    }
}

package com.savoirfairelinux.sflweather.ui.city_details;

import com.savoirfairelinux.sflweather.ui.base.MvpPresenter;

public interface CityDetailsMvpPresenter<V extends CityDetailsMvpView> extends MvpPresenter<V> {

    void onLoadCity(String cityId);
}

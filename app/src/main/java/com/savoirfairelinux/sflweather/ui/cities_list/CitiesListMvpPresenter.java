package com.savoirfairelinux.sflweather.ui.cities_list;

import android.content.Context;

import com.savoirfairelinux.sflweather.ui.base.MvpPresenter;

public interface CitiesListMvpPresenter<V extends CitiesListMvpView> extends MvpPresenter<V> {

    void onLoadCities(Context context);
}

package com.savoirfairelinux.sflweather.ui.cities_list;

import android.support.annotation.IntRange;

import com.savoirfairelinux.sflweather.data.file.model.City;
import com.savoirfairelinux.sflweather.ui.base.MvpView;

import java.util.List;

public interface CitiesListMvpView extends MvpView {

    void updateProgress(@IntRange(from = 0) int lineLength);

    void onNext(City city);

    void onFileReadError();

    void setCityList(List<City> allCities);

    void setMaxProgressBar(int fileSize);
}

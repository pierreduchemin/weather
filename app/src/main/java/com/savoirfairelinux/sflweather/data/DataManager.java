package com.savoirfairelinux.sflweather.data;

import com.savoirfairelinux.sflweather.data.file.FileHelper;
import com.savoirfairelinux.sflweather.data.network.ApiHelper;

public interface DataManager extends FileHelper, ApiHelper {
}
package com.savoirfairelinux.sflweather.data.network;

import com.savoirfairelinux.sflweather.data.network.model.CityInfo;

import io.reactivex.Observable;

public class AppApiHelper implements ApiHelper {

    @Override
    public Observable<CityInfo> getWeatherByCityIdApiCall(String cityId) {
        return ApiServiceFactory.getOpenWeatherMapService().getWeatherByCityIdApiCall(cityId);
    }
}

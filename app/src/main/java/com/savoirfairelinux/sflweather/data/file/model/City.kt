package com.savoirfairelinux.sflweather.data.file.model

data class City(val id: String, val name: String) : Comparable<City> {

    override fun compareTo(other: City): Int {
        return name.toLowerCase().compareTo(other.name.toLowerCase())
    }

    override fun equals(other: Any?): Boolean {
        return other is City && other.id == id
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        return result
    }
}
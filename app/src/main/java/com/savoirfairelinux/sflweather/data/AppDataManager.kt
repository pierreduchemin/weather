package com.savoirfairelinux.sflweather.data

import android.content.Context

import com.savoirfairelinux.sflweather.data.file.AppFileHelper
import com.savoirfairelinux.sflweather.data.file.model.City
import com.savoirfairelinux.sflweather.data.network.AppApiHelper
import com.savoirfairelinux.sflweather.data.network.model.CityInfo

import io.reactivex.Observable

class AppDataManager : DataManager {

    private val appFileHelper: AppFileHelper
    private val appApiHelper: AppApiHelper

    init {
        appFileHelper = AppFileHelper()
        appApiHelper = AppApiHelper()
    }

    override fun getCitiesFromFile(context: Context): Observable<City> {
        return appFileHelper.getCitiesFromFile(context)
    }

    override fun getAllCities(): List<City> {
        return appFileHelper.allCities
    }

    override fun setAllCities(cities: List<City>) {
        appFileHelper.allCities = cities
    }

    override fun addCity(city: City?) {
        appFileHelper.addCity(city)
    }

    override fun isFileCompletelyLoaded(): Boolean {
        return appFileHelper.isFileCompletelyLoaded
    }

    override fun initializeFile(context: Context): Int {
        return appFileHelper.initializeFile(context)
    }

    override fun getFileLoadingProgress(): Int {
        return appFileHelper.fileLoadingProgress
    }

    override fun getWeatherByCityIdApiCall(cityId: String): Observable<CityInfo> {
        return appApiHelper.getWeatherByCityIdApiCall(cityId)
    }
}

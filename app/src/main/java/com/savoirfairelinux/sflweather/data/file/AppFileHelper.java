package com.savoirfairelinux.sflweather.data.file;

import android.content.Context;

import com.savoirfairelinux.sflweather.R;
import com.savoirfairelinux.sflweather.data.file.model.City;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

public class AppFileHelper implements FileHelper {

    private List<City> allCities = new ArrayList<>();
    private boolean isFileCompletelyLoaded = false;
    private int fileSize = 0;
    private InputStream inputStream;
    private int fileLoadingPogress = 0;

    @Override
    public int initializeFile(Context context) {
        int cityListResource = R.raw.city_list;
        inputStream = context.getResources().openRawResource(cityListResource);
        try {
            fileSize = inputStream.available();
        } catch (IOException e) {
            Timber.e("onLoadCities(): not able to access file");
        }
        return fileSize;
    }

    @Override
    public Observable<City> getCitiesFromFile(Context context) {
        initializeFile(context);
        InputStreamReader inputReader = new InputStreamReader(inputStream);
        return Observable.create(emitter -> {
            try (BufferedReader bufferedReader = new BufferedReader(inputReader)) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    if (emitter.isDisposed()) {
                        Timber.i("getCitiesFromFile(): emitter is disposed. Finishing...");
                        emitter.onComplete();
                        return;
                    }

                    try {
                        City cityFromLine = getCityFromLine(line);
                        if (!allCities.contains(cityFromLine)) {
                            fileLoadingPogress += line.length();
                            emitter.onNext(cityFromLine);
                        }
                    } catch (InvalidCityException | IllegalArgumentException e) {
                        Timber.e("getCitiesFromFile(): %s", e.getMessage());
                    }
                }
                isFileCompletelyLoaded = true;
                emitter.onComplete();
            } catch (IOException e) {
                emitter.onError(e);
            } finally {
                inputStream.close();
            }
        });
    }

    @Override
    public List<City> getAllCities() {
        return allCities;
    }

    public void setAllCities(List<City> allCities) {
        this.allCities = allCities;
    }

    public boolean isFileCompletelyLoaded() {
        return isFileCompletelyLoaded;
    }

    @Override
    public int getFileLoadingProgress() {
        return fileLoadingPogress;
    }

    @Override
    public void addCity(City city) {
        if (city == null) {
            throw new IllegalArgumentException();
        }
        allCities.add(city);
    }

    private City getCityFromLine(String line) throws InvalidCityException {
        if (line == null) {
            throw new IllegalArgumentException();
        }

        String[] split = line.split("\\t");
        if (split.length < 5) {
            throw new InvalidCityException("Invalid city on line: " + line);
        }

        String id = split[0];
        if (id.isEmpty()) {
            throw new InvalidCityException("Invalid city id on line: " + line);
        }

        String name = split[1];
        if (name.isEmpty()) {
            throw new InvalidCityException("Invalid city name on line: " + line);
        }

        return new City(id, name);
    }
}

package com.savoirfairelinux.sflweather.data.network.model;

public enum Units {

    METRIC("metric"),
    IMPERIAL("imperial");

    public String name;

    Units(String name) {
        this.name = name;
    }
}

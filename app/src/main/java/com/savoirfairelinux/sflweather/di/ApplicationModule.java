package com.savoirfairelinux.sflweather.di;

import android.app.Application;
import android.content.Context;

import com.savoirfairelinux.sflweather.MvpApp;
import com.savoirfairelinux.sflweather.data.AppDataManager;
import com.savoirfairelinux.sflweather.data.DataManager;
import com.savoirfairelinux.sflweather.ui.cities_list.CitiesListMvpPresenter;
import com.savoirfairelinux.sflweather.ui.cities_list.CitiesListMvpView;
import com.savoirfairelinux.sflweather.ui.cities_list.CitiesListPresenter;
import com.savoirfairelinux.sflweather.ui.city_details.CityDetailsMvpPresenter;
import com.savoirfairelinux.sflweather.ui.city_details.CityDetailsMvpView;
import com.savoirfairelinux.sflweather.ui.city_details.CityDetailsPresenter;
import com.savoirfairelinux.sflweather.utils.rx.AppSchedulerProvider;
import com.savoirfairelinux.sflweather.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ApplicationModule {

    private final MvpApp mvpApp;

    public ApplicationModule(MvpApp mvpApp) {
        this.mvpApp = mvpApp;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mvpApp;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mvpApp;
    }

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new AppDataManager();
    }


    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    CitiesListMvpPresenter<CitiesListMvpView> provideCitiesListPresenter(
            CitiesListPresenter<CitiesListMvpView> presenter) {
        return presenter;
    }

    @Provides
    CityDetailsMvpPresenter<CityDetailsMvpView> provideCityDetailsPresenter(
            CityDetailsPresenter<CityDetailsMvpView> presenter) {
        return presenter;
    }
}
package com.savoirfairelinux.sflweather.di;

import com.savoirfairelinux.sflweather.data.DataManager;
import com.savoirfairelinux.sflweather.ui.cities_list.CitiesListActivity;
import com.savoirfairelinux.sflweather.ui.city_details.CityDetailsFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    DataManager provideDataManager();

    // Liste tous les endroits où le component est utilisé
    void inject(CitiesListActivity citiesListActivity);

    void inject(CityDetailsFragment cityDetailsFragment);
}
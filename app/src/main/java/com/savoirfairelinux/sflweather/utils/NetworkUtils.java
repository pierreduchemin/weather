package com.savoirfairelinux.sflweather.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.net.MalformedURLException;
import java.net.URL;

import timber.log.Timber;


/**
 * Utility class to check network status
 */
public class NetworkUtils {

    @Nullable
    private static ConnectivityManager connectivityManager;

    private NetworkUtils() {
    }

    @Nullable
    private static ConnectivityManager getConnectivityManager(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        if (connectivityManager == null) {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager == null) {
                Timber.e("Can't get wifi manager from context");
            }
        }
        return connectivityManager;
    }

    @Nullable
    private static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager connectivityManager = getConnectivityManager(context);
        if (connectivityManager == null) {
            return null;
        }
        return connectivityManager.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity
     */
    public static boolean isConnected(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        return networkInfo != null && networkInfo.isConnected();
    }

    public static boolean isUrl(@NonNull String string) {
        try {
            new URL(string);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }
}
